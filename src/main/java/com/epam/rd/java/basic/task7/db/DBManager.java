package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.*;
import java.util.*;


import com.epam.rd.java.basic.task7.db.entity.*;

import static com.epam.rd.java.basic.task7.db.Queries.*;


public class DBManager {

    private static DBManager instance;


    public static synchronized DBManager getInstance() {
        if (instance == null)
            instance = new DBManager();
        return instance;
    }

    private DBManager() {
    }

    public static Connection buildConnection() throws DBException {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("app.properties"));
        } catch (IOException e) {
            e.printStackTrace();
            throw new DBException("file read error", e);
        }

        String url = properties.getProperty("connection.url");
        String driver = properties.getProperty("driver");

//        try {
//            Class.forName(driver);
//        } catch (ClassNotFoundException | NullPointerException e) {
//            e.printStackTrace();
//        }

        try {
            return DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("file read error", e);
        }
    }

    public List<User> findAllUsers() throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(SELECT_USERS);
            resultSet = preparedStatement.executeQuery();
            List<User> userList = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String login = resultSet.getString(2);
                User u = new User();
                u.setId(id);
                u.setLogin(login);
                userList.add(u);
            }
            return userList;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: findAllUsers()", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean insertUser(User user) throws DBException {
        List<User> userList = findAllUsers();
        boolean userExists = false;
        if (userList != null) {
            userExists = userList.contains(user);
        }
        if (userExists) return false;
        else {
            Connection connection = null;
            PreparedStatement preparedStatement = null;
            try {
                connection = buildConnection();
                preparedStatement = connection.prepareStatement(INSERT_USER);
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
                throw new DBException("Method: insertUser()", e);
            } finally {
                if (preparedStatement != null) {
                    try {
                        preparedStatement.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                }
            }
            return true;
        }
    }

    public boolean deleteUsers(User... users) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(DELETE_USER_BY_LOGIN);
            for (User u : users) {
                preparedStatement.setString(1, u.getLogin());
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: deleteUsers()", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public User getUser(String login) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(SELECT_USER_ID_BY_LOGIN);
            preparedStatement.setString(1, login);
            resultSet = preparedStatement.executeQuery();
            int id = 0;
            while (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            User u = new User();
            u.setId(id);
            u.setLogin(login);
            return u;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: getUser()", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Team getTeam(String name) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(SELECT_TEAM_ID_BY_NAME);
            preparedStatement.setString(1, name);
            resultSet = preparedStatement.executeQuery();
            int id = 0;
            while (resultSet.next()) {
                id = resultSet.getInt("id");
            }
            Team t = new Team();
            t.setId(id);
            t.setName(name);
            return t;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: getTeam()", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Team> findAllTeams() throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(SELECT_TEAMS);
            resultSet = preparedStatement.executeQuery();
            List<Team> teamList = new ArrayList<>();
            while (resultSet.next()) {
                int id = resultSet.getInt(1);
                String name = resultSet.getString(2);
                Team t = new Team();
                t.setId(id);
                t.setName(name);
                teamList.add(t);
            }
            return teamList;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: findAllTeams()", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(INSERT_TEAM);
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: insertTeam()", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = buildConnection();
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(INSERT_KEYS_USER_ID_TEAM_ID);
            for (Team t : teams) {
                int userId = getUser(user.getLogin()).getId();
                int teamsId = getTeam(t.getName()).getId();
                preparedStatement.setInt(1, userId);
                preparedStatement.setInt(2, teamsId);
                preparedStatement.executeUpdate();
            }
            connection.commit();
        } catch (SQLException e) {
            try {
                connection.rollback();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            throw new DBException("failed", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.setAutoCommit(true);
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;

    }

    public List<Team> getUserTeams(User user) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(SELECT_ALL_TEAM_ID_BY_USER_ID);
            int userId = getUser(user.getLogin()).getId();
            preparedStatement.setInt(1, userId);
            resultSet = preparedStatement.executeQuery();
            List<Integer> listTeamId = new ArrayList<>();
            while (resultSet.next()) {
                listTeamId.add(resultSet.getInt(2));
            }
            List<Team> teamList = new ArrayList<>();
            for (Integer teamId : listTeamId) {
                int id;
                String name;
                preparedStatement = connection.prepareStatement(SELECT_TEAM_BY_ID);
                preparedStatement.setInt(1, teamId);
                resultSet = preparedStatement.executeQuery();
                while (resultSet.next()) {
                    id = resultSet.getInt("id");
                    name = resultSet.getString("name");
                    Team t = new Team();
                    t.setId(id);
                    t.setName(name);
                    teamList.add(t);
                }
            }
            return teamList;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: getUserTeams()", e);
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        try {
            connection = buildConnection();
            preparedStatement= connection.prepareStatement(DELETE_TEAM_BY_NAME);
            preparedStatement.setString(1, team.getName());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: deleteTeam()", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        Team inDB = getTeam(team.getPrevState());
        try {
            connection = buildConnection();
            preparedStatement = connection.prepareStatement(UPDATE_TEAM_BY_ID);
            preparedStatement.setString(1, team.getName());
            preparedStatement.setInt(2, inDB.getId());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            throw new DBException("Method: updateTeam()", e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}



//package com.epam.rd.java.basic.task7.db;
//
//import java.sql.*;
//import java.util.ArrayList;
//import java.util.List;
//
//import com.epam.rd.java.basic.task7.DBUTILS;
//import com.epam.rd.java.basic.task7.db.entity.*;
//
//
//public class DBManager {
//
//    private static DBManager instance;
//
//    public static synchronized DBManager getInstance() {
//        if (instance == null) {
//            instance = new DBManager();
//        }
//        return instance;
//    }
//
//    private DBManager() {
//
//    }
//
//    public List<User> findAllUsers() throws DBException {
//        List<User> result = new ArrayList<>();
//        try (Connection con = DBUTILS.getConnection()) {
//            Statement stmt = con.createStatement();
//            ResultSet rs = stmt.executeQuery(DBCommands.SQL_FIND_ALL_USERS);
//
//            while (rs.next()) {
//                User temp = new User();
//                temp.setId(rs.getInt("id"));
//                temp.setLogin(rs.getString("login"));
//                result.add(temp);
//            }
//
//            return result;
//        } catch (SQLException e) {
//            throw new DBException("cannot findAllUsers", e);
//        }
//
//
//    }
//
//    public boolean insertUser(User user) throws DBException {
//
//        boolean res = false;
//        try(Connection con  = DBUTILS.getConnection()) {
//            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_INSERT_USER,Statement.RETURN_GENERATED_KEYS);
//
//            prstmt.setString( 1,user.getLogin());
//
//            if(prstmt.executeUpdate()>0){
//                ResultSet rs =prstmt.getGeneratedKeys();
//              if(rs.next()){
//                  user.setId(rs.getInt(1) );
//                  res=true;
//              }
//            }
//
//        } catch (SQLException e) {
//            throw new DBException("cannot insertUser", e);
//        }
//        return res;
//    }
//
//    public boolean deleteUsers(User... users) throws DBException {
//        return false;
//    }
//
//    public User getUser(String login) throws DBException {
//        try {
//            User user = new User();
//            Connection con = DBUTILS.getConnection();
//            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_FIND_USER_BY_LOGIN);
//
//            //якщо в prstmt є хоч один незаповнений плейсхолдер, то треба його заповнити
//            prstmt.setString(1, login);
//            ResultSet rs = prstmt.executeQuery();
////            if (rs.next()) {
////                user = DBUTILS.extractUser(rs);
////            }
//            int id = 0;
//            while (rs.next()) {
//                id = rs.getInt("id");
//            }
//            User u = new User();
//            u.setId(id);
//            u.setLogin(login);
//            return u;
//           // return user;
//        } catch (SQLException e) {
//            throw new DBException("cannot getUser", e);
//        }
//
//    }
//
//
//    public Team getTeam(String name) throws DBException {
////        try {
////            Team team = new Team();
////            Connection con = DBUTILS.getConnection();
////            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_FIND_TEAM_BY_NAME);
////
////            //якщо в prstmt є хоч один незаповнений плейсхолдер, то треба його заповнити
////            prstmt.setString(1, name);
////            ResultSet rs = prstmt.executeQuery();
////            if (rs.next()) {
////                team= DBUTILS.extractTeam(rs);
////            }
////            return team;
////        } catch (SQLException e) {
////            throw new DBException("cannot getTeam", e);
////        }
//        Connection connection = null;
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        try {
//            connection = DBUTILS.getConnection();
//            preparedStatement = connection.prepareStatement(DBCommands.SQL_FIND_TEAM_BY_NAME);
//            preparedStatement.setString(1, name);
//            resultSet = preparedStatement.executeQuery();
//            int id = 0;
//            while (resultSet.next()) {
//                id = resultSet.getInt("id");
//            }
//            Team t = new Team();
//            t.setId(id);
//            t.setName(name);
//            return t;
//        } catch (SQLException e) {
//            e.printStackTrace();
//            throw new DBException("Method: getTeam()", e);
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (preparedStatement != null) {
//                try {
//                    preparedStatement.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//
//    }
//    public Team getTeam(int id) throws DBException {
//        try {
//            Team team = new Team();
//            Connection con = DBUTILS.getConnection();
//            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_FIND_TEAM_BY_ID);
//
//            //якщо в prstmt є хоч один незаповнений плейсхолдер, то треба його заповнити
//            prstmt.setInt(1, id);
//            ResultSet rs = prstmt.executeQuery();
//            if (rs.next()) {
//                team= getTeam(rs.getInt("login"));
//            }
//            return team;
//        } catch (SQLException e) {
//            throw new DBException("cannot getTeam", e);
//        }
//    }
//    public List<Team> findAllTeams() throws DBException {
//        List<Team> result = new ArrayList<>();
//        try (Connection con = DBUTILS.getConnection()) {
//            Statement stmt = con.createStatement();
//            ResultSet rs = stmt.executeQuery(DBCommands.SQL_FIND_ALL_TEAMS);
//
//            while (rs.next()) {
//                Team temp = new Team();
//                temp.setId(rs.getInt("id"));
//                temp.setName(rs.getString("name"));
//                result.add(temp);
//            }
//
//            return result;
//        } catch (SQLException e) {
//            throw new DBException("cannot findAllUsers", e);
//        }
//    }
//
//    public boolean insertTeam(Team team) throws DBException {
//        boolean res = false;
//        try(Connection con  = DBUTILS.getConnection()) {
//            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_INSERT_TEAM,Statement.RETURN_GENERATED_KEYS);
//
//            prstmt.setString( 1,team.getName());
//
//            if(prstmt.executeUpdate()>0){
//                ResultSet rs =prstmt.getGeneratedKeys();
//                if(rs.next()){
//                    team.setId(rs.getInt(1) );
//                    res=true;
//                }
//            }
//
//        } catch (SQLException e) {
//            throw new DBException("cannot insertTeam", e);
//        }
//        return res;
//    }
//
//    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
//
//        boolean res = false;
//        Connection con  = null;
//
//
//
//        try {
//            con = DBUTILS.getConnection();
//            con.setAutoCommit(false);
//
//            for(Team team : teams){
//                PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_SET_TEAMS_FOR_USER);
//
//                prstmt.setInt( 1, user.getId());
//                prstmt.setInt( 2,team.getId());
//
//            }
//
//            con.commit();
//            res = true;
//        } catch (SQLException e) {
//            try {
//                con.rollback();
//            } catch (SQLException ex) {
//                ex.printStackTrace();
//            }
//            throw new DBException("cannot insertTeam", e);
//        }
//        return res;
//
//    }
//
//    public List<Team> getUserTeams(User user) throws DBException {
//
////        List<Team> res2 = new ArrayList<>();
////
////        try {
////            Connection con = DBUTILS.getConnection();
////            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_FIND_TEAMS_FOR_USER);
////
////            //якщо в prstmt є хоч один незаповнений плейсхолдер, то треба його заповнити
////            int userId = getUser(user.getLogin()).getId();
////            prstmt.setInt(1,userId);
////            ResultSet rs = prstmt.executeQuery();
////            while (rs.next()) {
////               res2.add(getTeam(rs.getInt(2)));
////            }
////            return res2;
////        } catch (SQLException e) {
////            throw new DBException("cannot getUser", e);
////        }
////
//
//        Connection connection = null;
//        PreparedStatement preparedStatement = null;
//        ResultSet resultSet = null;
//        try {
//            connection =DBUTILS.getConnection();
//            preparedStatement = connection.prepareStatement(DBCommands.SQL_FIND_TEAMS_FOR_USER);
//            int userId = getUser(user.getLogin()).getId();
//            preparedStatement.setInt(1, userId);
//            resultSet = preparedStatement.executeQuery();
//            List<Integer> listTeamId = new ArrayList<>();
//            while (resultSet.next()) {
//                listTeamId.add(resultSet.getInt(2));
//            }
//            List<Team> teamList = new ArrayList<>();
//            for (Integer teamId : listTeamId) {
//                int id;
//                String name;
//                preparedStatement = connection.prepareStatement(DBCommands.SELECT_TEAM_BY_ID);
//                preparedStatement.setInt(1, teamId);
//                resultSet = preparedStatement.executeQuery();
//                while (resultSet.next()) {
//                    id = resultSet.getInt("id");
//                    name = resultSet.getString("name");
//                    Team t = new Team();
//                    t.setId(id);
//                    t.setName(name);
//                    teamList.add(t);
//                }
//            }
//            return teamList;
//        } catch (SQLException e) {
//            e.printStackTrace();
//            throw new DBException("Method: getUserTeams()", e);
//        } finally {
//            if (resultSet != null) {
//                try {
//                    resultSet.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (preparedStatement != null) {
//                try {
//                    preparedStatement.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//            if (connection != null) {
//                try {
//                    connection.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//
//    }
//
//    public boolean deleteTeam(Team team) throws DBException {
//        boolean res = false;
//        try(Connection con  = DBUTILS.getConnection()) {
//            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_DELETE_TEAM);
//            prstmt.setInt(1,team.getId());
//            res = prstmt.executeUpdate()>0;
//        } catch (SQLException e) {
//            throw new DBException("cannot insertTeam", e);
//        }
//        return res;
//
//    }
//
//    public boolean updateTeam(Team team) throws DBException {
//
//        boolean res = false;
//        try(Connection con  = DBUTILS.getConnection()) {
//            PreparedStatement prstmt = con.prepareStatement(DBCommands.SQL_UPDATE_TEAM);
//
//            prstmt.setInt( 1,team.getId());
//            prstmt.setString( 2,team.getName());
//            res = prstmt.executeUpdate()>0;
//        } catch (SQLException e) {
//            throw new DBException("cannot insertTeam", e);
//        }
//        return res;
//    }
//
//
//}
