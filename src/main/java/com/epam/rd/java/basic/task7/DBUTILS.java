package com.epam.rd.java.basic.task7;

import com.epam.rd.java.basic.task7.db.entity.DBCommands;
import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class DBUTILS {

    public static void close(Connection con) {

        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    static public User extractUser(ResultSet rs) throws SQLException {
        User u = new User();
        u.setId(rs.getInt("id"));
        u.setLogin(rs.getString("login"));
        return u;
    }

    static public Team extractTeam(ResultSet rs) throws SQLException {
        Team u = new Team();
        u.setId(rs.getInt("id"));
        u.setName(rs.getString("name"));
        return u;
    }

    public static Connection getConnection() throws SQLException {
        String URL = null;
        try (InputStream inputStream = new FileInputStream("app.properties")) {
            Properties p = new Properties();
            p.load(inputStream);
            URL = p.getProperty("connection.url");

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    Connection con = DriverManager.getConnection(URL);
    // adjust con
//		con.setAutoCommit(false);
//		con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
        return con;

}

}
