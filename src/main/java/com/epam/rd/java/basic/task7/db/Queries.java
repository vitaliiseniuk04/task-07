package com.epam.rd.java.basic.task7.db;

public class Queries {

    // Table Users
    public static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    public static final String DELETE_USER_BY_LOGIN = "DELETE FROM users WHERE login=?";
    public static final String SELECT_USER_ID_BY_LOGIN = "SELECT id FROM users WHERE login = ?";
    public static final String SELECT_USERS = "SELECT * FROM users";

    // Table Teams
    public static final String INSERT_TEAM = "INSERT INTO teams(name) VALUES(?)";
    public static final String UPDATE_TEAM_BY_ID = "UPDATE teams SET name=? WHERE id=?";
    public static final String SELECT_TEAM_ID_BY_NAME = "SELECT id FROM teams WHERE name = ?";
    public static final String SELECT_TEAM_BY_ID = "SELECT * FROM teams WHERE id=?";
    public static final String SELECT_TEAMS = "SELECT * FROM teams";
    public static final String DELETE_TEAM_BY_NAME = "DELETE FROM teams WHERE name = ?";

    // Table Users_Teams
    public static final String INSERT_KEYS_USER_ID_TEAM_ID = "INSERT INTO users_teams VALUES(?, ?)";
    public static final String SELECT_ALL_TEAM_ID_BY_USER_ID = "SELECT * FROM users_teams WHERE user_id=?";




}
