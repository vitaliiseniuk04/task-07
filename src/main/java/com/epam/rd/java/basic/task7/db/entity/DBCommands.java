package com.epam.rd.java.basic.task7.db.entity;

public class DBCommands {
    public static final String CONNECTION_URL = "jdbc:mysql://10.7.0.9:3307/testdb?user=testuser&password=testpass";

    public static final String SQL_FIND_ALL_USERS = "select * from users";
    public static final String SQL_FIND_USER_BY_LOGIN = "SELECT id FROM users WHERE login = ?";
    public static final String SQL_INSERT_USER ="INSERT INTO users VALUES (default,?)";

    public static final String SQL_FIND_ALL_TEAMS = "select * from teams";
    public static final String SQL_FIND_TEAM_BY_NAME = "SELECT id FROM teams WHERE name = ?";
    public static final String SQL_FIND_TEAM_BY_ID ="select login FROM teams WHERE id = ?" ;
    public static final String SQL_INSERT_TEAM = "INSERT INTO teams VALUES  (default,?)";
    public static final String SELECT_TEAM_BY_ID = "SELECT * FROM teams WHERE id=?";

    public static final String SQL_SET_TEAMS_FOR_USER = "insert into users_teams values (?,?)";
    public static final String SQL_FIND_TEAMS_FOR_USER2 = "SELECT * FROM users_teams WHERE user_id=?";
    public static final String SQL_FIND_TEAMS_FOR_USER = "SELECT * FROM users_teams WHERE user_id=?";
    public static final String SQL_UPDATE_TEAM = "update teams set name = ? where id = ?";
    public static final String SQL_DELETE_TEAM = "delete from teams where id =?";


}
